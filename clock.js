window.onload = function () {
    var canvas = document.getElementById("clock");
    var context = canvas.getContext('2d');
    var width = canvas.width;
    var height = canvas.height;
    // 比例
    var rem = Math.round(width / 400);

    // 移动坐标到canvas中心
    context.translate(width / 2, height / 2);

    //画时钟背景
    function drawBackground() {

        //画圆

        context.beginPath();
        context.fillStyle = "#fff";
        context.lineWidth = 1;
        var radius = width / 2 - context.lineWidth / 2;
        context.arc(0, 0, radius, 0, 2 * Math.PI);
        context.fill();

        // 画表盘
        context.beginPath();
        // 1、画小时数
        //var hoursArr = [3,4,5,6,7,8,9,10,11,12,1,2];
        // 罗马数字 Ⅰ、Ⅱ、Ⅲ、Ⅳ、Ⅴ、Ⅵ、Ⅶ、Ⅷ、Ⅸ、Ⅹ、Ⅺ、Ⅻ
        var hoursArr = ["Ⅲ", "Ⅳ", "Ⅴ", "Ⅵ", "Ⅶ", "Ⅷ", "Ⅸ", "Ⅹ", "Ⅺ", "Ⅻ", "Ⅰ", "Ⅱ"];


        var hoursRadius = radius - 38 * rem; // 小时数的内圆半径
        context.fillStyle = "#000";
        context.font = "lighter " + (30 * rem) + "px Arial";
        context.textAlign = "center";
        context.textBaseline = "middle";

        for (var i = 0; i < 12; i++) {
            context.save();
            var x = Math.cos(2 * Math.PI * i / 12) * hoursRadius;
            var y = Math.sin(2 * Math.PI * i / 12) * hoursRadius;
            context.translate(x, y);
            // 小时数向圆外排列
            if (i < 10) {
                context.rotate((i * 30 + 90) * Math.PI / 180);
            } else {
                context.rotate((i - 9) * 30 * Math.PI / 180);
            }
            context.fillText(hoursArr[i], 0, 0, 16);

            context.restore();
        }




        // 2、画刻度

        for (var i = 0; i < 60; i++) {

            context.beginPath();

            // 整小时对应的是原点，其它为线条
            if (i % 5 === 0) {
                var x = Math.cos(2 * Math.PI * i / 60) * (radius - 15);
                var y = Math.sin(2 * Math.PI * i / 60) * (radius - 15);
                context.fillStyle = "#000";
                context.arc(x, y, 2, 0, 2 * Math.PI);
                context.fill();
            } else {
                var x1 = Math.cos(2 * Math.PI * i / 60) * (radius - 12);
                var y1 = Math.sin(2 * Math.PI * i / 60) * (radius - 12);
                var x2 = Math.cos(2 * Math.PI * i / 60) * (radius - 18);
                var y2 = Math.sin(2 * Math.PI * i / 60) * (radius - 18);
                context.strokeStyle = "#000";
                context.lineWidth = 1;
                context.lineCap = "butt";
                context.moveTo(x1, y1);
                context.lineTo(x2, y2);
                context.stroke();
            }

        }



    }

    // 时针
    function drawHours(hours, minutes) {
        if (hours >= 3) {
            hours -= 3;
        } else {
            hours += 9;
        }

        // 小时的坐标还要根据分钟额外加上
        var opt = 2 * Math.PI * 30 * minutes / 360 / 60;

        var angle = 2 * Math.PI * hours / 12 + opt; //弧度

        var x = Math.cos(angle) * width / 3;
        var y = Math.sin(angle) * width / 3;

        var _r = 1 / 3 * width / 3 * Math.cos(5 * Math.PI / 180);

        var x1 = Math.cos(angle - 5 * Math.PI / 180) * _r;
        var y1 = Math.sin(angle - 5 * Math.PI / 180) * _r;

        var x2 = Math.cos(angle + 5 * Math.PI / 180) * _r;
        var y2 = Math.sin(angle + 5 * Math.PI / 180) * _r;

        context.beginPath();

        //context.lineWidth = 1;
        context.fileStyle = "#000";
        context.moveTo(0, 0);
        context.lineTo(x1, y1);
        context.lineTo(x, y);
        context.lineTo(x2, y2);
        context.lineTo(0, 0);
        context.fill();

    }

    // 分针
    function drawMinutes(minutes) {
        if (minutes >= 15) {
            minutes -= 15;
        } else {
            minutes += 60 - 15;
        }

        var angle = 2 * Math.PI * minutes / 60; //弧度

        var x = Math.cos(angle) * width / 2 / 1.1;
        var y = Math.sin(angle) * width / 2 / 1.1;

        var _r = 1 / 3 * width / 2 / 1.1 * Math.cos(5 * Math.PI / 180);

        var x1 = Math.cos(angle - 5 * Math.PI / 180) * _r;
        var y1 = Math.sin(angle - 5 * Math.PI / 180) * _r;

        var x2 = Math.cos(angle + 5 * Math.PI / 180) * _r;
        var y2 = Math.sin(angle + 5 * Math.PI / 180) * _r;

        context.beginPath();

        //context.lineWidth = 1;
        context.fillStyle = "#000";
        context.moveTo(0, 0);
        context.lineTo(x1, y1);
        context.lineTo(x, y);
        context.lineTo(x2, y2);
        context.lineTo(0, 0);
        context.fill();
    }

    // 画秒针
    function drawSeconds(seconds) {
        if (seconds >= 15) {
            seconds -= 15;
        } else {
            seconds += 60 - 15;
        }
        var angle = 2 * Math.PI * seconds / 60; //弧度

        var x1 = Math.cos(angle) * (width / 2 - 27);
        var y1 = Math.sin(angle) * (width / 2 - 27);
        var x2 = Math.cos(angle) * (width / 2 - 12);
        var y2 = Math.sin(angle) * (width / 2 - 12);
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = "#000";
        context.moveTo(0, 0);
        context.lineTo(x1, y1);
        context.stroke();
        context.beginPath();
        context.strokeStyle = "red";
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();

    }

    // 画表盘中心原点
    function drawCenterPointer() {
        context.beginPath();
        context.fillStyle = "#ccc";
        context.arc(0, 0, 4, 0, 2 * Math.PI);
        context.fill();
        context.strokeStyle = "#000";
        context.lineWidth = 2;
        context.arc(0, 0, 5, 0, 2 * Math.PI);
        context.stroke();
    }



    function draw() {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();
        context.clearRect(-width / 2, -height / 2, width, height);
        drawBackground();
        drawHours(h, m);
        drawMinutes(m);
        drawSeconds(s);
        drawCenterPointer();
    }

    draw();
    setInterval(draw, 1000);

}
